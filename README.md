Computer simulation of electric field lines
===========================================

**Note that this is just the two-dimensional slice of a three-dimensional field
in the plane of the charges.  The field line density is not proportional to the
electric field stregth.  See
<https://en.wikipedia.org/wiki/Field_line#Precise_definition>, and also
<https://en.wikipedia.org/wiki/Field_line#cite_note-2> for more problems with
these diagrams.**
