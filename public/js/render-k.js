
var sim1 = new simulation();
var c = document.getElementById("myCanvas");

function allowDrop(ev)
{
	"use strict";
	ev.preventDefault();
}

function drag(ev)
{
	"use strict";
	ev.dataTransfer.setData("Text",ev.target.id);
}

function drop(ev)
{
	"use strict";
	sim1.drop(ev);
}

function addCharge(cx,cy,cq)
{
	"use strict";
	sim1.addCharge(cx,cy,cq);
}

function clearCharges()
{
	"use strict";
	sim1.clearCharges();
}

function deleteCharge()
{
	"use strict";
	sim1.deleteCharge();
}

function setRedraw(val)
{
	"use strict";
	sim1.setRedraw(val);
}

function setCharge(val)
{
	"use strict";
	sim1.setCharge(val);
}

c.addEventListener('mousemove',sim1.mouseMove);

c.addEventListener('mousedown',sim1.mouseDown);

c.addEventListener('mouseup', sim1.mouseUp);

