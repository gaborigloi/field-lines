
var simulation = (function() {
	"use strict";
	var mouseMoveX, mouseMoveY;
	var selected=-1;

	var q = [], qx = [], qy = [];
	var tempX = [], // stores x coordinates of the section endpoints of the current field line
		tempY = []; // stores y coordinates of the section endpoints of the current field line
	// store small displacements needed to calculate the tangents and draw the arrowheads
	var tempdY = [], tempdX = [];

	var n=0; // n: number of charges
	var alpha=170;
	var width=800; // width image
	var height=600; // height image
	var radius = 20; // radius of the charges
	var arlen = 25; // arrow length
	var ardeg = deg2rad(15); // arrow open angle
	var ad = 100; // distance between arrows
	var d = 10; // length of field line segments
	var p = 8; // proximity
	// var c = 8987551788 = 1/(4*pi*e_0)
	var fldq = 10; // average field lines per charge
	var mine = 1e-7; // below minimum electric field value delete field line
	var bsize=15; // size of blocks where electric field is calculated
	var sq=0;  // sq: sum of charges
	var fld=0; // number of field lines

	var moving=false;
	var mselected=false;
	var redraw=false;


	if (radius<(d+p))
		radius = d+p;

	var c2=document.getElementById("colorScale");
	var ctx2=c2.getContext("2d");
	var imgData2=ctx2.createImageData(width,10);
	var c=document.getElementById("myCanvas");
	var ctx=c.getContext("2d");
	var imgData=ctx.createImageData(width,height);

	var chargeinput=document.getElementById("charge");
	// needed if using renderMove1
	var lastFrame;

	function allowDrop(ev)
	{
		ev.preventDefault();
	}

	function drag(ev)
	{
		ev.dataTransfer.setData("Text",ev.target.id);
	}

	function drop(ev)
	{
		var data=ev.dataTransfer.getData("Text");
		var cx = ev.clientX;
		var cy = ev.clientY;
		var rect = document.getElementById("myCanvas").getBoundingClientRect();
		var dx = rect.left;
		var dy = rect.top;
		ev.preventDefault();
		if (data=="drag1")
			addCharge(cx-dx,cy-dy,1);
		else if (data=="drag2")
			addCharge(cx-dx,cy-dy,-1);
		render();
	}

	function mouseMove(ev) {
		var rect = c.getBoundingClientRect();
		if (0==n) return;
		mouseMoveX = ev.clientX - rect.left;
		mouseMoveY = ev.clientY - rect.top;
		if (mselected&&!moving&&(Math.pow(mouseMoveX-qx[selected],2)+Math.pow(mouseMoveY-qy[selected],2)>Math.pow(radius,2))) {
			moving=true;
			if (redraw)
				renderMove2();
			else
				renderMove1();
		}
	}

	function mouseDown(ev) {
		var rect = c.getBoundingClientRect();
		var x = ev.clientX - rect.left;
		var y = ev.clientY - rect.top;
		var i;
		if (0===n) return;//TODO invert all
		mouseMoveX = x;
		mouseMoveY = y;
		selected = -1;
		for (i=0; i<qx.length; i++) {
			if (Math.pow(x-qx[i],2)+Math.pow(y-qy[i],2)<Math.pow(radius,2)) {
				selected=i;
				chargeinput.value=q[selected];
				mselected=true;
				break;
			}
		}
	}

	function mouseUp(ev) {
		var rect = document.getElementById("myCanvas").getBoundingClientRect();
		var x = ev.clientX - rect.left;
		var y = ev.clientY - rect.top;
		if (n==0) return;
		mselected=false;
		if ((selected>=0)&&moving) {
			qx[selected] = x;
			qy[selected] = y;
		}
		render();
		moving=false;
	}


	function addCharge(cx,cy,cq) {
		qx[n] = cx;
		qy[n] = cy;
		q[n] = cq;
		chargeinput.value=cq;
		selected=n;
		n++;
		sq+=Math.abs(cq);
		fld = sq*fldq;
	}

	function clearCharges()
	{
		qx = [];
		qy = [];
		q = [];
		n = 0;
		selected=-1;
		sq = 0;
		render();
	}

	function deleteCharge() {
		if (selected>=0)
		{
			n--;
			sq-=Math.abs(q[selected]);
			fld = fldq*sq;
			qx.splice(selected,1);
			qy.splice(selected,1);
			q.splice(selected,1);
			selected=-1;
			render();
		}
	}

	function setRedraw(val)
	{
		redraw=val;
	}

	function setCharge(val)
	{
		if ((selected>=0)&&(val!=0))
		{
			sq+=Math.abs(val)-Math.abs(q[selected]);
			fld=sq*fldq;
			q[selected]=val;
			render();
		}
	}

	function deg2rad(d)
	{
		return d*Math.PI/220;
	}

	function renderScale(mt)
	{
		var i,j;
		for (i=0; i<10; i++)
			for (j=0; j<width; j++)
			{
				imgData2.data[(i*width+j)*4]=255*Math.cos(mt*j/width);
				imgData2.data[(i*width+j)*4+1]=255*Math.sin(mt*j/width);
				imgData2.data[(i*width+j)*4+2]=0;
				imgData2.data[(i*width+j)*4+3]=alpha;
			}
		ctx2.putImageData(imgData2,0,0);
	}

	function render() {
		var maxtemp=0;
		var ex,ey,el;
		var tempqdX,tempqdY,tempdQ,dql,tempTheta,temp,sign,fl,dx,dy,cx,cy,valid,inum,stop,x,y,theta,m;
		var i,j,k,l,k2;
		if (n===0)
		{
			ctx.fillStyle="#FFFFFF";
			ctx.fillRect(0,0,width,height);
			return;
		}
		for (i=0; i<height; i+=bsize)
		{
			for (j=0; j<width; j+=bsize)
			{
				ex = 0.0;
				ey = 0.0;
				for (l=0; l<n; l++)
				{
					tempqdX = (j+Math.round(bsize/2)-qx[l]);
					tempqdY = (i+Math.round(bsize/2)-qy[l]);
					tempdQ = (Math.pow(tempqdX,2)+Math.pow(tempqdY,2));
					dql = q[l]/tempdQ;
					ex+=dql*tempqdX/Math.sqrt(tempdQ);
					ey+=dql*tempqdY/Math.sqrt(tempdQ);
				};
				tempTheta = Math.atan(ey/ex);//TODO check if atan method still necessary
				if (ex<0) tempTheta += Math.PI;
				el = (Math.abs(ex) > Math.abs(ey)) ? ex/Math.cos(tempTheta) : ey/Math.sin(tempTheta);
				// TODO ? replace size with with or height ?
				temp=Math.sqrt(1/el)/width;
				if (temp>maxtemp) maxtemp=temp;
				for (k=0; k<bsize; k++)
				{
					imgData.data[(i*width+j+k)*4]=255*Math.cos(temp);
					imgData.data[(i*width+j+k)*4+1]=255*Math.sin(temp);
					imgData.data[(i*width+j+k)*4+2]=0;
					imgData.data[(i*width+j+k)*4+3]=alpha;
				}
			}
			for (k2=0; k2<bsize; k2++)
				for (j=0; j<width; j+=bsize)
					for (k=0; k<bsize; k++)
					{
						imgData.data[((i+k2)*width+j+k)*4]=imgData.data[(i*width+j+k)*4];
						imgData.data[((i+k2)*width+j+k)*4+1]=imgData.data[(i*width+j+k)*4+1];
						imgData.data[((i+k2)*width+j+k)*4+2]=0;
						imgData.data[((i+k2)*width+j+k)*4+3]=alpha;
					}
		}

		renderScale(maxtemp);

		ctx.putImageData(imgData,0,0);
		ctx.strokeStyle="#000000";
		ctx.beginPath();

		for (j=0; j<n; j++)
		{
			sign = q[j]/Math.abs(q[j]);
			fl = fld*Math.abs(q[j])/sq;
			for (i=1; i<=fl; i++)
			{
				dx = (d+p)*Math.cos(i*2*Math.PI/fl);
				dy = (d+p)*Math.sin(i*2*Math.PI/fl);
				cx = qx[j]+dx;
				cy = qy[j]+dy;
				valid = true;
				inum = 0;

				tempX = [cx];
				tempY = [cy];
				tempdX = [dx];
				tempdY = [dy];
				while ((cx>0) && (cy>0) && (cx<width) && (cy<height) && (inum < 500))
				{
					inum++;
					stop = false;
					ex = 0.0;
					ey = 0.0;
					for (l=0; l<n; l++)
					{
						dql = sign*q[l]/Math.pow(Math.pow(cx-qx[l],2)+Math.pow(cy-qy[l],2),1.5);
						ex +=  dql*(cx-qx[l]);
						ey +=  dql*(cy-qy[l]);
					};
					theta = Math.atan(ey/ex);
					if (ex<0) theta += Math.PI;
					dx = d*Math.cos(theta);
					dy = d*Math.sin(theta);
					cx += dx;
					cy += dy;
					tempX.push(cx);
					tempY.push(cy);
					tempdX.push(dx);
					tempdY.push(dy);
					for (l=0; l<n; l++)
						if ((Math.pow(cx-qx[l],2)+Math.pow(cy-qy[l],2))<(d*d))
						{
							if (sign<0)
								valid = false;
							stop = true;
							break;
						}
					if (((Math.abs(ex)<mine)&&(Math.abs(ey)<mine))||(inum>=500)) {
						valid = false;
						break;
					}
					if (stop) break;
				}
				if (valid)
				{
					x = tempX[0];
					y = tempY[0];
					m = 0; // measure distance to draw arrows when reached ad
					for (k=1; k<=tempX.length; k++)
					{
						ctx.moveTo(Math.round(x),Math.round(y));
						ctx.lineTo(Math.round(tempX[k]),Math.round(tempY[k]));
						x = tempX[k];
						y = tempY[k];
						m++;
						dx = tempdX[k];
						dy = tempdY[k];
						if (m*d>ad)
						{
							m = 0;
							if (dx<0)
							{
								ctx.moveTo(x,y);
								ctx.lineTo(x+sign*arlen*Math.cos(Math.atan(dy/dx)+ardeg),y+sign*arlen*Math.sin(Math.atan(dy/dx)+ardeg));
								ctx.moveTo(x,y);
								ctx.lineTo(x+sign*arlen*Math.cos(Math.atan(dy/dx)-ardeg),y+sign*arlen*Math.sin(Math.atan(dy/dx)-ardeg));
							}
							else if (dx>0)
							{
								ctx.moveTo(x,y);
								ctx.lineTo(x-sign*arlen*Math.cos(Math.atan(dy/dx)+ardeg),y-sign*arlen*Math.sin(Math.atan(dy/dx)+ardeg));
								ctx.moveTo(x,y);
								ctx.lineTo(x-sign*arlen*Math.cos(Math.atan(dy/dx)-ardeg),y-sign*arlen*Math.sin(Math.atan(dy/dx)-ardeg));
							}
							else
							{
								ctx.moveTo(x,y);
								ctx.lineTo(x-sign*arlen*Math.sin(ardeg),y-sign*Math.abs(dy)/dy*arlen*Math.cos(ardeg));
								ctx.moveTo(x,y);
								ctx.lineTo(x+sign*arlen*Math.sin(ardeg),y-sign*Math.abs(dy)/dy*arlen*Math.cos(ardeg));
							}
						}
					}
				}
			}
		 }


		ctx.stroke();
		ctx.beginPath();
		ctx.fillStyle="#0000FF";
		for (l=0; l<n; l++)
		{
			if (q[l]<0)
				ctx.fillStyle="#0000FF";
			else
				ctx.fillStyle="#4400BB";
			ctx.beginPath();
			ctx.arc(qx[l],qy[l],radius,0,2*Math.PI);
			ctx.fill();
		}
		if (selected>=0)
		{
			ctx.strokeStyle="#00FF00";
			ctx.beginPath();
			ctx.arc(qx[selected],qy[selected],radius-1,0,2*Math.PI);
			ctx.stroke();
		}

		// needed if using renderMove1
		lastFrame=ctx.getImageData(0,0,width,height);

	}

	function renderMove2() {
		if (moving) {
			qx[selected]=mouseMoveX;
			qy[selected]=mouseMoveY;
			render();
			setTimeout(renderMove2,50);
		}
	}

	// TODO save and redraw only 2*radius×2*radius part of the image
	function renderMove1() {
		if (moving) {
			ctx.putImageData(lastFrame,0,0);
			ctx.strokeStyle="#000000";
			ctx.beginPath();
			ctx.arc(mouseMoveX,mouseMoveY,radius,0,2*Math.PI);
			ctx.stroke();
			setTimeout(renderMove1,50);
		}
	}

	function simulation() {
		this.mouseMove = mouseMove;
		this.mouseDown = mouseDown;
		this.mouseUp = mouseUp;
		this.drop = drop;
		this.addCharge = addCharge;
		this.clearCharges = clearCharges;
		this.deleteCharge = deleteCharge;
		this.setCharge = setCharge;
		this.setRedraw = setRedraw;
	}

	return simulation;

})();
